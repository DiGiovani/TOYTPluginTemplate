package com.toyt.androidplugin;

import android.app.Activity;
import android.text.TextUtils;
import android.os.Handler;
import android.widget.Toast;

public class AndroidPlugin {
    private final Activity GameActivity;

    public static native void NativeInitialize();

    public AndroidPlugin(Activity GameActivity) {
        this.GameActivity = GameActivity;
        NativeInitialize();
    }

    public void OnDestroy() {
    }

    public void ShowToast(String message) {
        GameActivity.runOnUiThread(new Runnable() {
            public void run() {
                CharSequence toastMessage = message;
                int duration = Toast.LENGTH_LONG;
                Toast.makeText(GameActivity, toastMessage, duration).show();
            }
        });
    }

    public void ChangeScreenOrientation(int orientation) {
        GameActivity.setRequestedOrientation(orientation);
    }
}