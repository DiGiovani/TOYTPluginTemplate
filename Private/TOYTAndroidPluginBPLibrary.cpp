// Copyright Epic Games, Inc. All Rights Reserved.

#include "TOYTAndroidPluginBPLibrary.h"
#include "TOYTAndroidPlugin.h"
// #define PLATFORM_ANDROID true

#if PLATFORM_ANDROID
#include "Android/AndroidApplication.h"
#include "Android/AndroidJNI.h"

static jmethodID Toast_MethodID;
static jmethodID ChangeScreenOrientation_MethodID;


// This is used to find java method ID, to enable us to call it from cpp file.
// This is a helper method. It takes as argument the JNI Env, the function name and its JNI Signature.
// Learn more about JNI Signature here: https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/types.html
static jmethodID FindMethod(JNIEnv *Env, const char *Name,
                            const char *Signature) {
  if (Env && Name && Signature) {
    return Env->GetMethodID(FJavaWrapper::GameActivityClassID, Name, Signature);
  }

  return nullptr;
}

// The methods below are used to call the methods. It takes as argument the JNI Env, the methodID (that the function above finds) and the needed arguments
// These methods wraps the JNI Functions (https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/functions.html) to prevent errors.
// Each method expects a different return from functions. Besides the basic java types and void, every return is object-type.

static bool CallBooleanMethod(JNIEnv *Env, jmethodID Method, ...) {
  // make sure the function exists
  jobject Object = FJavaWrapper::GameActivityThis;
  if (Method == NULL || Object == NULL || Env == NULL) {
    return false;
  }

  va_list Args;
  va_start(Args, Method);
  return Env->CallBooleanMethodV(Object, Method, Args);
  va_end(Args);
}

static bool CallIntMethod(JNIEnv *Env, jmethodID Method, ...) {
  // make sure the function exists
  jobject Object = FJavaWrapper::GameActivityThis;
  if (Method == NULL || Object == NULL || Env == NULL) {
    return false;
  }

  va_list Args;
  va_start(Args, Method);
  return Env->CallIntMethodV(Object, Method, Args);
  va_end(Args);
}

static void CallVoidMethod(JNIEnv *Env, jmethodID Method, ...) {
  // make sure the function exists
  jobject Object = FJavaWrapper::GameActivityThis;
  if (Method == NULL || Object == NULL || Env == NULL) {
    return;
  }

  va_list Args;
  va_start(Args, Method);
  Env->CallVoidMethodV(Object, Method, Args);
  va_end(Args);
}

static jobject CallObjectMethod(JNIEnv *Env, jmethodID Method) {
  // make sure the function exists
  jobject Object = FJavaWrapper::GameActivityThis;
  if (Method == NULL || Object == NULL || Env == NULL) {
    return NULL;
  }

  return Env->CallObjectMethod(Object, Method);
}

#endif


UTOYTAndroidPluginBPLibrary::UTOYTAndroidPluginBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

void UTOYTAndroidPluginBPLibrary::TOYTAndroidPluginToast(const FString &message)
{
#if PLATFORM_ANDROID
    if (JNIEnv *Env = FAndroidApplication::GetJavaEnv()) {
        auto JToastMessage = FJavaHelper::ToJavaString(Env, message);
        return CallVoidMethod(Env, Toast_MethodID, *JToastMessage);
    }
#endif
}

void UTOYTAndroidPluginBPLibrary::TOYTAndroidPLuginOrientation(const ETOYTOrientationCode orientation)
{
#if PLATFORM_ANDROID
    if (JNIEnv *Env = FAndroidApplication::GetJavaEnv()) {
        return CallVoidMethod(Env, ChangeScreenOrientation_MethodID, (int)orientation);
    }
#endif
}





#if PLATFORM_ANDROID
// This function must be called in java side on constructor method. It is used to find methods and class fields.
// For class fields, you must pass classes as argument.
// Ex: ...NativeInitialize(JNIEnv *Env, jobject, Thiz, jclass ClassName) {
//      ClassName_FieldName_FieldID = FindClass(Env, ClassName, "FieldName", "JNISignature");
// }

JNI_METHOD void
Java_com_toyt_androidplugin_AndroidPlugin_NativeInitialize(JNIEnv *Env, jobject Thiz) {
    Toast_MethodID = FindMethod(Env, "AndroidThunkJava_ShowToast", "(Ljava/lang/String;)V");
    ChangeScreenOrientation_MethodID = FindMethod(Env, "AndroidThunkJava_ChangeScreenOrientation", "(I)V");
}
#endif