// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "TOYTAndroidPluginBPLibrary.generated.h"

/* 
*	Function library class.
*	Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.
*
*	When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.
*	BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.
*	BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.
*	DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.
*				Its lets you name the node using characters not allowed in C++ function names.
*	CompactNodeTitle - the word(s) that appear on the node.
*	Keywords -	the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu. 
*				Good example is "Print String" node which you can find also by using keyword "log".
*	Category -	the category your node will be under in the Blueprint drop-down menu.
*
*	For more info on custom blueprint nodes visit documentation:
*	https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation
*/

UENUM(BlueprintType)
enum class ETOYTOrientationCode : uint8 {
    BEHIND = 3 UMETA(DisplayName = "Behind"),
    FULL_SENSOR = 10 UMETA(DisplayName="Full Sensor"),
    FULL_USER = 13 UMETA(DisplayName="Full User"),
    LANDSCAPE = 0 UMETA(DisplayName="Landscape"),
    LOCKED = 14 UMETA(DisplayName="Locked"),
    NOSENSOR = 5 UMETA(DisplayName="No Sensor"),
    PORTRAIT = 1 UMETA(DisplayName="Portrait"),
    REVERSE_LANDSCAPE = 8 UMETA(DisplayName="Reverse Landscape"),
    REVERSE_PORTRAIT = 9 UMETA(DisplayName="Reverse Portrait"),
    SENSOR = 4 UMETA(DisplayName="Sensor"),
    SENSOR_LANDSCAPE = 6 UMETA(DisplayName="Sensor Landscape"),
    SENSOR_PORTRAIT = 7 UMETA(DisplayName= "Sensor Portrait"),
    UNSPECIFIED = (uint8) -1 UMETA(DisplayName="Unspecified"),
    USER = 2 UMETA(DisplayName="User"),
    USER_LANDSCAPE = 11 UMETA(DisplayName="User Landscape"),
    USER_PORTRAIT = 12 UMETA(DisplayName="User Portrait"),
};

UCLASS()
class UTOYTAndroidPluginBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Toast", Keywords = "Toast "), Category = "TOYT")
	static void TOYTAndroidPluginToast(const FString &message);

    UFUNCTION(BlueprintCallable, meta = (DisplayName = "Change orientation", Keywords = "TOYT Change Orientation"), Category="TOYT")
    static void TOYTAndroidPLuginOrientation(const ETOYTOrientationCode orientation);
};
